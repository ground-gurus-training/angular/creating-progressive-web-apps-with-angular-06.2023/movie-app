import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { FavoritesService } from '../services/favorites.service';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
})
export class FavoritesComponent implements OnInit {
  favorites$?: Observable<any>;

  constructor(private favoritesService: FavoritesService) {}

  ngOnInit() {
    this.favorites$ = this.favoritesService.getFavorites();
  }
}
