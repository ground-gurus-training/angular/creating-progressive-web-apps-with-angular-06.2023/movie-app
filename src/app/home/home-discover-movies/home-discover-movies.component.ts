import { Component, OnInit } from '@angular/core';
import { MovieService } from 'src/app/services/movie.service';

@Component({
  selector: 'app-home-discover-movies',
  templateUrl: './home-discover-movies.component.html',
  styleUrls: ['./home-discover-movies.component.scss'],
})
export class HomeDiscoverMoviesComponent implements OnInit {
  discoverResults: any[] = [];

  constructor(private movieService: MovieService) {}

  ngOnInit() {
    this.loadMovies();
  }

  loadMovies() {
    this.movieService.discoverMovies().subscribe((movie) => {
      this.discoverResults = movie.results;
    });
  }
}
