import { Component, OnInit } from '@angular/core';
import { MovieService } from '../services/movie.service';
import { Movie } from '../model/movie';
import { FormControl, FormGroup } from '@angular/forms';
import { FavoritesService } from '../services/favorites.service';
import { Favorite } from '../model/favorite';
import {
  DocumentData,
  QueryDocumentSnapshot,
  QuerySnapshot,
} from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  searchForm: FormGroup = new FormGroup({
    query: new FormControl(''),
  });
  searchedMovies: Movie[] = [];
  favorites$?: Observable<any>;

  constructor(
    private favoritesService: FavoritesService,
    private movieService: MovieService
  ) {}

  ngOnInit() {
    this.favorites$ = this.favoritesService.getFavorites();
    // favorites.subscribe((favorite: DocumentData[]) => {
    //   favorite.forEach((doc: DocumentData) => {
    //     this.favoriteMovies.push({
    //       movie_id: doc['movie_id'],
    //       title: doc['title'],
    //     });
    //   });
    // });
  }

  async addToFavorites(movie: Movie) {
    const fav: QuerySnapshot<DocumentData> =
      await this.favoritesService.getFavoriteByTitle(movie.title);
    if (fav.docs.length === 0) {
      this.favoritesService.addFavorite({
        movie_id: movie.id,
        title: movie.title,
      });

      movie.isFavorite = true;
    } else {
      const fav = await this.favoritesService.getFavoriteByTitle(movie.title);

      const docs = fav.docs;

      if (docs.length > 0) {
        const doc = docs[0];
        this.favoritesService.deleteFavorite(doc.id);
        movie.isFavorite = false;
      }
    }
  }

  searchMovie() {
    this.movieService
      .search(this.searchForm.get('query')?.value)
      .subscribe(async (data) => {
        if (!data) return; // guard clause

        this.searchedMovies = [];

        if (this.favorites$) {
          this.favorites$.subscribe((favorite: DocumentData[]) => {
            const favoriteMovies: Favorite[] = [];

            favorite.forEach((doc: DocumentData) => {
              favoriteMovies.push({
                movie_id: doc['movie_id'],
                title: doc['title'],
              });
            });

            for (let result of data.results) {
              const lookupMovie = favoriteMovies.find(
                (fav: Favorite) => fav.movie_id === result.id
              );

              const movie: Movie = {
                id: result.id,
                title: result.original_title,
                overview: result.overview,
                posterPath: result.poster_path,
                isFavorite: lookupMovie ? true : false,
              };
              this.searchedMovies.push(movie);
            }
          });
        }
      });
  }
}
