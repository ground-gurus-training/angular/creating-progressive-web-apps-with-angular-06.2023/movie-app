import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { ReactiveFormsModule } from '@angular/forms';

import { CardModule } from 'primeng/card';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';

import { HomeDiscoverMoviesComponent } from './home-discover-movies/home-discover-movies.component';

@NgModule({
  declarations: [HomeComponent, HomeDiscoverMoviesComponent],
  imports: [
    CardModule,
    InputTextModule,
    ButtonModule,
    ReactiveFormsModule,
    CommonModule,
  ],
  exports: [HomeComponent],
})
export class HomeModule {}
