import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingComponent } from './landing.component';

import { ButtonModule } from 'primeng/button';

@NgModule({
  declarations: [LandingComponent],
  imports: [ButtonModule, CommonModule],
})
export class LandingModule {}
