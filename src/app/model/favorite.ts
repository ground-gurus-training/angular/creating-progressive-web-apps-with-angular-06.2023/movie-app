export interface Favorite {
  id?: string;
  movie_id: string;
  title: string;
}
