import { Injectable } from '@angular/core';
import {
  DocumentData,
  Firestore,
  QuerySnapshot,
  addDoc,
  collection,
  collectionChanges,
  collectionData,
  deleteDoc,
  doc,
  docData,
  getDoc,
  getDocs,
  query,
  where,
} from '@angular/fire/firestore';
import { Favorite } from '../model/favorite';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class FavoritesService {
  constructor(private firestore: Firestore) {}

  getFavorites() {
    const colRef = collection(this.firestore, 'favorites');
    return collectionData(colRef);
  }

  getFavorite(id: string) {
    const ref = doc(this.firestore, `favorites/${id}`);
    return getDoc(ref);
  }

  getFavoriteByMovieId(id: string) {
    const q = query(
      collection(this.firestore, 'favorites'),
      where('movie_id', '==', id)
    );
    return getDocs(q);
  }

  getFavoriteByTitle(title: string): Promise<QuerySnapshot<DocumentData>> {
    const q = query(
      collection(this.firestore, 'favorites'),
      where('title', '==', title)
    );
    return getDocs(q);
  }

  addFavorite(favorite: Favorite) {
    const ref = collection(this.firestore, 'favorites');
    return addDoc(ref, this.setUndefinedValuesToNull(favorite));
  }

  deleteFavorite(id: string) {
    const ref = doc(this.firestore, `favorites/${id}`);
    return deleteDoc(ref);
  }

  setUndefinedValuesToNull(data: any) {
    Object.keys(data)
      .filter((k) => data[k] == undefined)
      .forEach((k) => (data[k] = null));
    return data;
  }
}
